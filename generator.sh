#!/usr/bin/env bash

set -eu

echo
echo "Snowball!"
echo
echo "What should we call your snowball (e.g. tom, george, beowulf)?"

read -r name

if [[ -z "$name" ]]; then
  name="beowulf"
fi

echo 'Your snowball will be named "'"$name"'"'
echo "Where should $name live (e.g.  ./$name)?"
echo

read -r path

if [[ -z "$path" ]]; then
  path="./$name"
fi

mkdir -p "$path"
git clone "https://gitlab.com/fresheyeball/snowball" "$path" || exit 1

echo "Naming snowball $name …"
echo

# sed differs slightly between GNU, BSD, and POSIX
# https://riptutorial.com/sed/topic/9436/bsd-macos-sed-vs--gnu-sed-vs--the-posix-sed-specification
xOsReplace() {
  if ! sed --version | grep -q "(GNU sed)"; then
    xargs -0 sed -i '' -e "s/snowball/$name/g"
  else
    xargs -0 sed -i -e "s/snowball/$name/g"
  fi
}

# Find and replace snowman in all files except the .git directory and README.md
find "$path" \( -type d -name .git -prune \) -o -type f ! -name README.md -print0 | xOsReplace

mv "$path/snowball.cabal" "$path/$name.cabal"

echo "Sweeping up …"
echo
rm -rf "$path/.git"
mv "$path/README.md.gen" "$path/README.md"
rm $path/*.sh
rm "$path/.gitlab-ci.yml"
rm "$path/LICENSE"

cat "$path/success.gen"
rm "$path/success.gen"

cd "$path"
